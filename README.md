## USAGE

The styles were generated with TailwindCSS CLI. 

## Production
`NODE_ENV=production npx tailwindcss -o styles.css --jit --purge="./index.php"`

## Dev
`npx tailwindcss -o styles.css --watch --jit --purge="./index.php"`