<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="https://gitlab.com/mottodesignstudio/motto-brand/-/raw/master/dist/img/motto-icon.png">
    <link rel="stylesheet" href="styles.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@500;800&display=swap" rel="stylesheet">  
    <style>
    body { font-family: 'Poppins', sans-serif; }
    </style>
    <title>Motto</title>
</head>
<body class="font-medium flex h-screen w-screen items-center justify-center flex-col bg-gradient-to-b from-[#f4fdfe] to-[#1bd5fa] text-[#1d163e]">
    <div class="flex-1 flex flex-col justify-center text-center">
        <h1 class="text-xl mb-5">
            <a href="https://motto.ca/products/domains/" class="inline-block uppercase tracking-widest bg-white rounded-full px-6 py-2 text-[#7E779E] hover:bg-[#7E779E] hover:text-white transition font-black">
                Parked Domain
                <svg xmlns="http://www.w3.org/2000/svg" class="inline-block h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14" />
                </svg>                
            </a>
        </h1>
        <span class="text-5xl font-black text-5xl"><?php echo $_SERVER['HTTP_HOST'] ?></span>
    </div>
    <div class="bg-[#ebfaff] w-full p-12">
        <div class="container flex flex-col sm:flex-row items-center mx-auto space-y-4 sm:space-y-0 sm:space-x-4">
            <a href="https://motto.ca" class="hover:text-[#15cdaa] transition sm:mr-auto">
                <svg viewbox="0 0 476 108" class="w-36" xmlns="http://www.w3.org/2000/svg"><path d="M306.7 84.2v21.2h-53.6V0H278v19.7h23v20h-22.9v44.5h28.6zm39.7 0V39.7h22.9v-20h-22.9V0h-24.9v105.4H375V84.2h-28.6zM237.9 63.1c0 24.8-20.7 44.9-46.3 44.9-25.6 0-46.3-20.1-46.3-44.9 0-24.8 20.8-44.9 46.3-44.9 25.5 0 46.3 20.1 46.3 44.9zm-25-.1c0-12.9-9.6-23.3-21.4-23.3-11.8 0-21.4 10.5-21.4 23.3s9.6 23.3 21.4 23.3c11.8.1 21.4-10.4 21.4-23.3zm263 .1c0 24.8-20.7 44.9-46.3 44.9-25.6 0-46.3-20.1-46.3-44.9 0-24.8 20.8-44.9 46.3-44.9 25.6 0 46.3 20.1 46.3 44.9zm-25-.1c0-12.9-9.6-23.3-21.4-23.3-11.8 0-21.4 10.5-21.4 23.3s9.6 23.3 21.4 23.3c11.9.1 21.4-10.4 21.4-23.3zM123.1 26.2c-5.1-5.3-12.2-8-21.2-8-5.9 0-11.1 1.2-15.7 3.5s-8.7 5.7-12.5 10.2c-2.2-4.4-5.4-7.8-9.5-10.1-4.1-2.4-9-3.5-14.6-3.5-3.1 0-5.9.4-8.4 1.1-2.5.8-4.8 1.8-6.8 3.1s-3.9 2.8-5.5 4.4c-1.7 1.7-3.2 3.4-4.6 5.2V19.9H0v85.6h24.3v-61h28.9v61h24.3v-61h28.9v61h24.3V49.7c.1-10.4-2.5-18.2-7.6-23.5z" fill="currentColor"/></svg>
            </a>
            <a class="flex items-center px-3 py-2 border border-transparent shadow-sm text-sm leading-4 font-medium rounded-md text-white bg-[#15cdaa] hover:bg-blue-900 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-800" href="tel:(514) 846-9889">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-3" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 3h5m0 0v5m0-5l-6 6M5 3a2 2 0 00-2 2v1c0 8.284 6.716 15 15 15h1a2 2 0 002-2v-3.28a1 1 0 00-.684-.948l-4.493-1.498a1 1 0 00-1.21.502l-1.13 2.257a11.042 11.042 0 01-5.516-5.517l2.257-1.128a1 1 0 00.502-1.21L9.228 3.683A1 1 0 008.279 3H5z" />
                </svg>                    
                (514) 846-9889
            </a>
            <a class="flex items-center px-3 py-2 border border-transparent shadow-sm text-sm leading-4 font-medium rounded-md text-white bg-[#15cdaa] hover:bg-blue-900 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-800" href="mail:hello@motto.ca">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-3" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
            </svg>    
                hello@motto.ca
            </a>
        </div>    
    </div>
</body>
</html>